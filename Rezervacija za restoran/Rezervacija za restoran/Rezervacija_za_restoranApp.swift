//
//  Rezervacija_za_restoranApp.swift
//  Rezervacija za restoran
//
//  Created by student on 23.01.2024..
//

import SwiftUI

@main
struct Rezervacija_za_restoranApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
